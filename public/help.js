// @ts-ignore
// const marked = window.marked;
marked.setOptions({ gfm: true, breaks: true });

// Configuration
// Maps snowflakes to PCMR channels, bots, and roles.
const config = {
    serverID: "77710284621357056",
    channels: {
        "135645254014468096": "#programming",
        "213077219424206858": "#linux",
        "213077236683636736": "#mac-os",
        "360863183625256971": "#free-games",
        "497150397358800908": "#build-help",
        "500820755391709194": "#overclocking",
        "537354814665785355": "#roles",
        "750325966936473600": "#hardware-discussion",
        "77710284621357056": "#main",
        "96028420848230400": "#tech-support",
        "96037009314836480": "#off-topic-memes-and-stuff",
    },
    users: {
        "355619002569195520": "@Modmail",
        "630852264780890135": "@PCMRBot",
    },
    roles: {
        "530366060181061645": "@Free games",
        "229748333114163200": "@Techies",
        "497150309425086472": "@Builders",
        "500846169019383828": "@Overclockers",
        "229748381159915520": "@Coders",
        "350093631459426304": "@Trusted Techies",
        "428731209213018114": "@Official Builder",
        "398477463933419521": "@TSQuiet",
    },
};

// Marked JS extension
// Adds __underline__ and parses <@userid> and <#channelid> tags
const discordFlavoredMarkdown = {
    extensions: [
        {
            name: "underline",
            level: "inline",
            start: (src) => src.match(/__/)?.index,
            tokenizer(src) {
                const rule = /^__(.*)__/;
                const match = rule.exec(src);
                if (match) {
                    return {
                        type: "underline",
                        raw: match[0],
                        text: this.lexer.inlineTokens(match[1]),
                    };
                }
            },
            renderer(token) {
                return `<u>${this.parser.parseInline(token.text)}</u>`;
            },
        },
        {
            name: "discordUsername",
            level: "inline",
            start: (src) => src.match(/<@/)?.index,
            tokenizer(src) {
                const rule = /^<@(\d+)>/;
                const match = rule.exec(src);
                if (match) {
                    let id = match[1];
                    return {
                        type: "discordUsername",
                        raw: match[0],
                        id,
                        name: config.users[id] ?? `@user ${id}`,
                        href: `https://discord.com/users/${id}`,
                    };
                }
            },
            renderer: (token) =>
                `<a href="${token.href}" class="discord_replace" target="_blank">${token.name}</a>`,
        },
        {
            name: "discordEveryoneHere",
            level: "inline",
            start: (src) => src.match(/(@everyone|@here)/)?.index,
            tokenizer(src) {
                const rule = /^(@everyone|@here)/;
                const match = rule.exec(src);
                if (match) {
                    return {
                        type: "discordEveryoneHere",
                        raw: match[0],
                        txt: match[1]
                    };
                }
            },
            renderer: (token) =>
                `<span class="discord_replace">${token.txt}</span>`,
        },
        {
            name: "discordChannel",
            level: "inline",
            start: (src) => src.match(/<#/)?.index,
            tokenizer(src, tokens) {
                const rule = /^<#(\d+)>/;
                const match = rule.exec(src);
                if (match) {
                    let id = match[1];
                    return {
                        type: "discordChannel",
                        raw: match[0],
                        id,
                        name: config.channels[id] ?? `#channel ${id}`,
                        href: `https://discord.com/channels/${config.serverID}/${id}`,
                    };
                }
            },
            renderer: (token) =>
                `<a href="${token.href}" class="discord_replace" target="_blank">${token.name}</a>`,
        },
        {
            name: "discordRole",
            level: "inline",
            start: (src) => src.match(/<@&/)?.index,
            tokenizer(src, tokens) {
                const match = /^<@&(\d+)>/.exec(src);
                if (match) {
                    let id = match[1];
                    return {
                        type: "discordRole",
                        raw: match[0],
                        name: config.roles[id] ?? `@role ${id}`,
                    };
                }
            },
            renderer: (token) =>
                `<span class="discord_replace" target="_blank">${token.name}</span>`,
        },
    ],
};
marked.use(discordFlavoredMarkdown);

// @ts-ignore
const $ = window.jQuery;

// Get jQuery elements...
const $footer = $("#wrapper tfoot");
const $loading = $("#loading");
const $searchInput = $("#search_string");
const $commandTable = $("#wrapper table tbody");
const $numberOfCommands = $("#number_of_commands");

// On pageload, hide footer, show loading message.
$footer.hide();
$loading.show();

/**
 * @typedef {{ command: string, name?: string, image?: string, aliases?: string[], text?: string, attribs?: string[] }} Command
 */
/**
 * Get and parse commands.json Filter out hidden commands.
 * @return {Promise<Command[]>}
 */
async function getCommands() {
    const commands = await fetch("commands.json?t=" + Date.now());
    const commands_data = await commands.json();
    const command_list = Object.entries(commands_data)
        .map(([cmd, data]) => ({ ...data, command: cmd }))
        .filter(data => !data.attribs?.includes("hidden"));

    const builtins = await fetch("builtins.json?t=" + Date.now());
    const builtins_data = await builtins.json();
    const builtin_list = Object.entries(builtins_data)
        .map(([cmd, data]) => ({ ...data, command: cmd }))
        .filter(data => !data.attribs?.includes("hidden"));

    /* Combine and sort the two lists together */
    return command_list.concat(builtin_list)
                       .sort((a,b) => (a.command.toLowerCase() > b.command.toLowerCase()) ? 1 : ((b.command.toLowerCase() > a.command.toLowerCase()) ? -1 : 0))

}
/**
 * Appends the list of commands to the $commandTable
 * @param {Command[]} commands
 */
function buildTable(commands) {
    // Build table
    for (const command of commands) {
        let imgSrc = command.image;
        if (imgSrc?.startsWith("/")) {
            imgSrc = `https://commands.discord.eegras.com/${imgSrc}`;
        }
        let aliases = command.aliases
            ? `<br><span class="aliases">${command.aliases.join("<br>")}</span>`
            : ``;
        let name = command.name
            ? `<div class="command_name">${marked.parseInline(command.name)}</div>`
            : ``;
        let txt = command.text
            ? `<div class="command_text">${marked.parse(command.text)}</div>`
            : ``;
        let img = command.image
            ? `<div class="command_image"><img src="${imgSrc}" href="${imgSrc}" class="image-lightbox"/></div>`
            : ``;
        const SHOW_FLAGS = false; // debug value?
        let flags = SHOW_FLAGS && command.attribs ? command.attribs.join("<br />") : ``;
        $commandTable.append(
            `<tr>
            <td class="command">
                <a name="${command.command.toLowerCase()}" class="anchor"></a>
                <a href="#${command.command.toLowerCase()}">${command.command.toLowerCase()}</a>
                ${aliases}
                <div class="flags">${flags}</div>
            </td>
            <td class="name">${name}${txt}${img}</td>
        </tr>`
        );
    }
}

// Handle search input
const handleSearchInput = () => {
    let visibleCommands = 0;
    const searchString = $searchInput.val().toLowerCase();
    $commandTable.find("tr").each(function () {
        if ($(this).text().toLowerCase().includes(searchString)) {
            visibleCommands += 1;
            $(this).show();
        } else {
            $(this).hide();
        }
    });
    if (visibleCommands == 0) {
        $footer.show();
    } else {
        $footer.hide();
    }
};
$searchInput.on("input", handleSearchInput);

// Invoke getCommands
(async function () {
    const commands = await getCommands();
    buildTable(commands);
    $searchInput.trigger("focus");
    $numberOfCommands.html(commands.length);
    $(".image-lightbox").featherlight({ type: "image" });
    $loading.hide();
    if (document.location.hash != "") {
        const targetEl = document.querySelector(`a[name="${document.location.hash.substring(1)}"]`);
        targetEl?.scrollIntoView(true);
    }
    handleSearchInput(); // call once on pageload
})();
